package com.example.gotrain.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.gotrain.R;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    private final static String DEPARTURE_CITY = "DEPARTURE_CITY";
    private final static String ARRIVAL_CITY = "ARRIVAL_CITY";

    private TextInputLayout departue;
    private TextInputLayout arrival;
    private Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        departue = findViewById(R.id.departure_textField);
        arrival = findViewById(R.id.arrival_textField);
        next = findViewById(R.id.next_button);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String departureCity = departue.getEditText().getText().toString();
                String arrivalCity = arrival.getEditText().getText().toString();

                boolean fieldsAreFilled = !(departureCity.isEmpty() && arrivalCity.isEmpty());
                boolean DepartureCityAndArrivalCityAreDifferent = !departureCity.equals(arrivalCity);

                if(fieldsAreFilled && DepartureCityAndArrivalCityAreDifferent){
                    Intent ResultsIntent = new Intent(MainActivity.this, ResultsActivity.class);

                    ResultsIntent.putExtra(DEPARTURE_CITY,departue.getEditText().getText().toString());
                    ResultsIntent.putExtra(ARRIVAL_CITY,arrival.getEditText().getText().toString());

                    startActivity(ResultsIntent);
                }
            }
        });
    }
}