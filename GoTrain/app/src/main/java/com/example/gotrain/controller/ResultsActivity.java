package com.example.gotrain.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.gotrain.R;
import com.example.gotrain.adapter.ResultItemAdapter;
import com.example.gotrain.models.ResultItem;

import java.util.ArrayList;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    private final static String DEPARTURE_CITY = "DEPARTURE_CITY";
    private final static String ARRIVAL_CITY = "ARRIVAL_CITY";

    private TextView departureTextView;
    private TextView arrivalTextView;
    private ListView resultsListView;

    private String cityDeparture;
    private String cityArrival;

    private List<ResultItem> resultItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        departureTextView = findViewById(R.id.departure_text_view);
        arrivalTextView = findViewById(R.id.arrival_text_view);
        resultsListView = findViewById(R.id.results_list_view);

        cityDeparture = getIntent().getStringExtra(DEPARTURE_CITY);
        cityArrival = getIntent().getStringExtra(ARRIVAL_CITY);

        departureTextView.setText(cityDeparture);
        arrivalTextView.setText(cityArrival);

        resultItemList = resultItemListData(cityDeparture, cityArrival);

        resultsListView.setAdapter(new ResultItemAdapter(ResultsActivity.this,resultItemList));

    }

    List<ResultItem> resultItemListData(String cityDeparture, String cityArrival){

        List<ResultItem> resultItemList = new ArrayList<>();

        resultItemList.add(new ResultItem("TER",cityDeparture,cityArrival,"05:51","08:58","38 €"));
        resultItemList.add(new ResultItem("TER",cityDeparture,cityArrival,"06:13","09:12","1 €"));
        resultItemList.add(new ResultItem("INTERCITES",cityDeparture,cityArrival,"07:06","09:18","21 €"));
        resultItemList.add(new ResultItem("TGV",cityDeparture,cityArrival,"08:44","11:02","44 €"));
        resultItemList.add(new ResultItem("INTERCITES",cityDeparture,cityArrival,"09:51","12:56","39 €"));
        resultItemList.add(new ResultItem("TER",cityDeparture,cityArrival,"11:51","14:57","31 €"));
        resultItemList.add(new ResultItem("TGV",cityDeparture,cityArrival,"12:33","14:33","55 €"));
        resultItemList.add(new ResultItem("TGV",cityDeparture,cityArrival,"13:41","15:27","61 €"));
        resultItemList.add(new ResultItem("INTERCITES",cityDeparture,cityArrival,"15:53","18:28","34 €"));
        resultItemList.add(new ResultItem("TER",cityDeparture,cityArrival,"17:14","20:45","19 €"));

        return resultItemList;
    }
}