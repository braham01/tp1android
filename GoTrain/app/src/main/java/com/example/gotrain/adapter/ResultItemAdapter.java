package com.example.gotrain.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.gotrain.R;
import com.example.gotrain.models.ResultItem;

import java.util.List;

public class ResultItemAdapter extends BaseAdapter {

    private Context context;
    private List<ResultItem> resultItemList;
    private LayoutInflater inflater;

    public ResultItemAdapter(Context context, List<ResultItem> resultItemList) {
        this.context = context;
        this.resultItemList = resultItemList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return resultItemList.size();
    }

    @Override
    public ResultItem getItem(int index) {
        return resultItemList.get(index);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.item_adapter, null);

        ResultItem currentResultItem = getItem(i);

        TextView nameTextView = view.findViewById(R.id.name_text_view);
        nameTextView.setText(currentResultItem.getName());

        TextView departueCityTextView = view.findViewById(R.id.departure_city_textView);
        departueCityTextView.setText(currentResultItem.getCityDeparture());

        TextView arrivalCityTextView = view.findViewById(R.id.arrival_city_textView);
        arrivalCityTextView.setText(currentResultItem.getCityArrival());

        TextView departueTimeTextView = view.findViewById(R.id.departure_time_textView);
        departueTimeTextView.setText(currentResultItem.getDateDeparture());

        TextView arrivalTimeTextView = view.findViewById(R.id.arrival_time_textView);
        arrivalTimeTextView.setText(currentResultItem.getDateArrival());

        TextView priceTextView = view.findViewById(R.id.price_textView);
        priceTextView.setText(currentResultItem.getPrice());

        return view;
    }
}
