package com.example.gotrain.models;

public class ResultItem {

    private String name;
    private String cityDeparture;
    private String cityArrival;
    private String dateDeparture;
    private String dateArrival;
    private String price;


    public ResultItem(String name, String cityDeparture, String cityArrival, String dateDeparture, String dateArrival, String price) {
        this.name = name;
        this.cityDeparture = cityDeparture;
        this.cityArrival = cityArrival;
        this.dateDeparture = dateDeparture;
        this.dateArrival = dateArrival;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityDeparture() {
        return cityDeparture;
    }

    public void setCityDeparture(String cityDeparture) {
        this.cityDeparture = cityDeparture;
    }

    public String getCityArrival() {
        return cityArrival;
    }

    public void setCityArrival(String cityArrival) {
        this.cityArrival = cityArrival;
    }

    public String getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(String dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public String getDateArrival() {
        return dateArrival;
    }

    public void setDateArrival(String dateArrival) {
        this.dateArrival = dateArrival;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
