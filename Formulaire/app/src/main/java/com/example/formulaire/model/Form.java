package com.example.formulaire.model;

import java.io.Serializable;

public class Form implements Serializable {

    private String name;
    private String firstName;
    private String age;
    private String areasOfExpertise;
    private String phoneNumber;

    public Form() {
        this.name = "";
        this.firstName = "";
        this.age = "";
        this.areasOfExpertise = "";
        this.phoneNumber = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAreasOfExpertise() {
        return areasOfExpertise;
    }

    public void setAreasOfExpertise(String areasOfExpertise) {
        this.areasOfExpertise = areasOfExpertise;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Form{" +
                "name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age='" + age + '\'' +
                ", areasOfExpertise='" + areasOfExpertise + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
