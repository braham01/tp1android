package com.example.formulaire.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.formulaire.R;
import com.example.formulaire.model.Form;

public class CheckFormActivity extends AppCompatActivity {

    private final static String FORM_CONTENT = "FORM_CONTENT";
    private final static String PHONE_NUMBER = "PHONE_NUMBER";

    private Form form;

    private TextView nomSaisiTextView;
    private TextView prenomSaisiTextView;
    private TextView ageSaisiTextView;
    private TextView competenceSaisiTextView;
    private TextView telephoneSaisiTextView;
    private Button confirmerButton;
    private Button retourButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_form);

        form = (Form) getIntent().getSerializableExtra("FORM_CONTENT");

        nomSaisiTextView = findViewById(R.id.check_form_textView_nom_saisi);
        prenomSaisiTextView = findViewById(R.id.check_form_textView_prenom_saisi);
        ageSaisiTextView = findViewById(R.id.check_form_textView_age_saisi);
        competenceSaisiTextView = findViewById(R.id.check_form_textView_competence_saisi);
        telephoneSaisiTextView = findViewById(R.id.check_form_textView_telephone_saisi);
        confirmerButton = findViewById(R.id.check_form_button_valider);
        retourButton = findViewById(R.id.check_form_button_retour);

        nomSaisiTextView.setText(form.getName());
        prenomSaisiTextView.setText(form.getFirstName());
        ageSaisiTextView.setText(form.getAge());
        competenceSaisiTextView.setText(form.getAreasOfExpertise());
        telephoneSaisiTextView.setText(form.getPhoneNumber());

        confirmerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent LaunchCallIntent = new Intent(CheckFormActivity.this, LaunchCallActivity.class);
                LaunchCallIntent.putExtra(PHONE_NUMBER, form.getPhoneNumber());
                startActivity(LaunchCallIntent);
            }
        });

        retourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}