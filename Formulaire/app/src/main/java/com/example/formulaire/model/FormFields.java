package com.example.formulaire.model;

public class FormFields {

    private Boolean nameField;
    private Boolean firstNameField;
    private Boolean ageField;
    private Boolean areasOfExpertiseField;
    private Boolean phoneNumberField;

    //on suppose que tous les champs sont vides par défaut
    public FormFields() {
        this.nameField = false;
        this.firstNameField = false;
        this.ageField= false;
        this.areasOfExpertiseField = false;
        this.phoneNumberField = false;
    }

    public void setNameFieldAsFilled() {
        this.nameField = true;
    }

    public void setNameFieldAsUnfilled() {
        this.nameField = false;
    }

    public void setFirstNameFieldAsFilled() {
        this.firstNameField = true;
    }

    public void setFirstNameFieldAsUnfilled() {
        this.firstNameField = false;
    }

    public void setAgeFieldAsFilled() {
        this.ageField = true;
    }

    public void setAgeFieldAsUnfilled() {
        this.ageField = false;
    }

    public void setAreasOfExpertiseFieldAsFilled() {
        this.areasOfExpertiseField = true;
    }

    public void setAreasOfExpertiseFieldAsUnfilled() {
        this.areasOfExpertiseField = false;
    }

    public void setPhoneNumberFieldAsFilled() {
        this.phoneNumberField = true;
    }

    public void setPhoneNumberFieldAsUnfilled() {
        this.phoneNumberField = false;
    }

    //vérifie si tous les champs ont étés remplis
    public boolean allFieldsAreFilled(){
        return  nameField &&
                firstNameField &&
                ageField &&
                areasOfExpertiseField &&
                phoneNumberField;
    }
}
