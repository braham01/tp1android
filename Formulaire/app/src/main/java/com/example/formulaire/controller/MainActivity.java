package com.example.formulaire.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.formulaire.R;
import com.example.formulaire.model.Form;
import com.example.formulaire.model.FormFields;

public class MainActivity extends AppCompatActivity {

    private final static String FORM_CONTENT = "FORM_CONTENT";

    private Form form;
    private FormFields formFields;

    private TextView demandeTextView;
    private EditText nomEditText;
    private EditText prenomEditText;
    private EditText ageEditText;
    private EditText competenceEditText;
    private EditText telephoneEditText;
    private Button validerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        demandeTextView = findViewById(R.id.main_textView_demande);
        nomEditText = findViewById(R.id.main_editText_nom);
        prenomEditText = findViewById(R.id.main_editText_prenom);
        ageEditText = findViewById(R.id.main_editText_age);
        competenceEditText = findViewById(R.id.main_editText_competence);
        telephoneEditText = findViewById(R.id.main_editText_telephone);
        validerButton = findViewById(R.id.main_button_valider);

        //désactivier le bouton de validation pour empecher l'envoie d'une formulaire vide
        validerButton.setEnabled(false);

        MainActivity thisIntent = this;

        // **** récupérer les valeurs entrées dans les champs + vérification si tous les champs ont été saisis

        //on va détecter si quelques chose a été saisie dans la zone de edit zone correspondante :
        //on construit notre formulaire au fur et mesure
        form = new Form();
        formFields = new FormFields();

        //1- nom
        nomEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){//si l'utilisateur remplit le champs
                    form.setName(nomEditText.getText().toString());//on met à jour notre formulaire
                    formFields.setNameFieldAsFilled();//on met à jour l'information sur le champs en question, ici le champs est remplit
                    if(formFields.allFieldsAreFilled()){//si tous les champs sont remplit on réactive le bouton de validation
                        validerButton.setEnabled(true);
                    }
                }
                else{//si l'utilisateur vide le champs
                    form.setName(""); //on met à jour notre formulaire, on vide notre tampon
                    formFields.setNameFieldAsUnfilled();//on note bien que le champs est vide
                    validerButton.setEnabled(false);//on désactive le bouton e validation
                }
            }
        });

        //2- prénom
        prenomEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){//si l'utilisateur remplit le champs
                    form.setFirstName(prenomEditText.getText().toString());//on met à jour notre formulaire
                    formFields.setFirstNameFieldAsFilled();//on met à jour l'information sur le champs en question, ici le champs est remplit
                    if(formFields.allFieldsAreFilled()){//si tous les champs sont remplit on réactive le bouton de validation
                        validerButton.setEnabled(true);
                    }
                }
                else{//si l'utilisateur vide le champs
                    form.setFirstName(""); //on met à jour notre formulaire, on vide notre tampon
                    formFields.setFirstNameFieldAsUnfilled();//on note bien que le champs est vide
                    validerButton.setEnabled(false);//on désactive le bouton e validation
                }
            }
        });

        //3- age
        ageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){//si l'utilisateur remplit le champs
                    form.setAge(ageEditText.getText().toString());//on met à jour notre formulaire
                    formFields.setAgeFieldAsFilled();//on met à jour l'information sur le champs en question, ici le champs est remplit
                    if(formFields.allFieldsAreFilled()){//si tous les champs sont remplit on réactive le bouton de validation
                        validerButton.setEnabled(true);
                    }
                }
                else{//si l'utilisateur vide le champs
                    form.setAge(""); //on met à jour notre formulaire, on vide notre tampon
                    formFields.setAgeFieldAsUnfilled();//on note bien que le champs est vide
                    validerButton.setEnabled(false);//on désactive le bouton e validation
                }
            }
        });

        //4 domaine de compétence
        competenceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){//si l'utilisateur remplit le champs
                    form.setAreasOfExpertise(competenceEditText.getText().toString());//on met à jour notre formulaire
                    formFields.setAreasOfExpertiseFieldAsFilled();//on met à jour l'information sur le champs en question, ici le champs est remplit
                    if(formFields.allFieldsAreFilled()){//si tous les champs sont remplit on réactive le bouton de validation
                        validerButton.setEnabled(true);
                    }
                }
                else{//si l'utilisateur vide le champs
                    form.setAreasOfExpertise(""); //on met à jour notre formulaire, on vide notre tampon
                    formFields.setAreasOfExpertiseFieldAsUnfilled();//on note bien que le champs est vide
                    validerButton.setEnabled(false);//on désactive le bouton e validation
                }
            }
        });

        //numéro de téléphone
        telephoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){//si l'utilisateur remplit le champs
                    form.setPhoneNumber(telephoneEditText.getText().toString());//on met à jour notre formulaire
                    formFields.setPhoneNumberFieldAsFilled();//on met à jour l'information sur le champs en question, ici le champs est remplit
                    if(formFields.allFieldsAreFilled()){//si tous les champs sont remplit on réactive le bouton de validation
                        validerButton.setEnabled(true);
                    }
                }
                else{//si l'utilisateur vide le champs
                    form.setPhoneNumber(""); //on met à jour notre formulaire, on vide notre tampon
                    formFields.setPhoneNumberFieldAsUnfilled();//on note bien que le champs est vide
                    validerButton.setEnabled(false);//on désactive le bouton e validation
                }
            }
        });


        validerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //on change le couleur de fond des champs de saisie
                //changeAllEditTextBackGroundColor("#9ACD32"); //yellowgreen

                //on crée et on build la fenetre de dialogue
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.validation_request);
                builder.setMessage(R.string.alert_message);
                builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // code exécuté lorsque l'utilisateur clique sur le bouton "OK"
                        Intent checkFormIntent = new Intent(MainActivity.this, CheckFormActivity.class);
                        checkFormIntent.putExtra(FORM_CONTENT, form);
                        startActivity(checkFormIntent);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // code exécuté lorsque l'utilisateur clique sur le bouton "Annuler"
                        //changeAllEditTextBackGroundColor(null);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    /*public void changeAllEditTextBackGroundColor(String colorString){
        Color color;
        if(colorString == null){
            color = null;
        }
        else{
            color = Color.parseColor(colorString);
        }
        nomEditText.setBackgroundColor();
        nomEditText.setBackgroundColor(Color.parseColor(color));
        prenomEditText.setBackgroundColor(Color.parseColor(color));
        ageEditText.setBackgroundColor(Color.parseColor(color));
        competenceEditText.setBackgroundColor(Color.parseColor(color));
        telephoneEditText.setBackgroundColor(Color.parseColor(color));
    } */
}

