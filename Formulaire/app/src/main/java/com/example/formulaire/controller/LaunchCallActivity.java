package com.example.formulaire.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.formulaire.R;

public class LaunchCallActivity extends AppCompatActivity {

    private final static String PHONE_NUMBER = "PHONE_NUMBER";
    private static final int REQUEST_CALL_PHONE_PERMISSION = 1;
    private String phoneNumber;

    private TextView phoneNumberTextView;
    private Button appelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_call);

       phoneNumber = getIntent().getStringExtra(PHONE_NUMBER);

        phoneNumberTextView = findViewById(R.id.launch_call_textView_phone);
        appelButton = findViewById(R.id.launch_call_button_call);

        phoneNumberTextView.setText(phoneNumber);

        appelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//on demande la pérmission de l'utilisateur, si on en dispose pas
                if (ContextCompat.checkSelfPermission(LaunchCallActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(LaunchCallActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE_PERMISSION);
                }else{//si on a déja la permssion, on lance l'appel
                    Intent callIntent = new Intent(Intent.ACTION_CALL);

                    callIntent.setData(Uri.parse("tel:" + phoneNumber));
                    startActivity(callIntent);
                }
            }
        });
    }

    //cette méthode sera appellé lorsqu'on a eu la permission de l'utilisateur
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CALL_PHONE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // La permission a été accordée, vous pouvez lancer un appel téléphonique ici
                Intent callIntent = new Intent(Intent.ACTION_CALL);

                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(callIntent);
            } else {
                // La permission a été refusée, informez l'utilisateur que votre application a besoin de cette permission pour fonctionner correctement
                Toast.makeText(this, "La permission d'appeler un numéro de téléphone est nécessaire pour utiliser cette fonctionnalité", Toast.LENGTH_LONG).show();
            }
        }
    }

}